//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Adelphatimer1._0.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Stories
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Stories()
        {
            this.Tasks = new HashSet<Tasks>();
            this.Sprints = new HashSet<Sprints>();
        }
    
        public int Id { get; set; }
        public string StoryName { get; set; }
        public int ResourcesId { get; set; }
        public string Importance { get; set; }
        public string InitialEstimate { get; set; }
        public string Demonstrate { get; set; }
        public string Notes { get; set; }
        public Nullable<int> CategoriesId { get; set; }
        public Nullable<System.DateTime> RequestDate { get; set; }
        public string RequestBy { get; set; }
        public Nullable<System.DateTime> ValidateDate { get; set; }
        public string ValidateBy { get; set; }
        public Nullable<bool> EnabledRow { get; set; }
        public System.DateTime CurrentDate { get; set; }
        public int ProjectsId { get; set; }
        public int StoryNumber { get; set; }
    
        public virtual Categories Categories { get; set; }
        public virtual Projects Projects { get; set; }
        public virtual Resources Resources { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Tasks> Tasks { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Sprints> Sprints { get; set; }
    }
}
