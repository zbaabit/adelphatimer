﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Adelphatimer1._0.Helpers
{
    public static class DateTimeExtenstions
    {
        public static int GetWorkingDays(this DateTime from, DateTime to)
        {
            var dayDifference = (int)to.Subtract(from).TotalDays;


            return Enumerable
                .Range(1, dayDifference)
                .Select(x => from.AddDays(x))
                .Count(x => x.DayOfWeek != DayOfWeek.Saturday && x.DayOfWeek !=DayOfWeek.Sunday)+1;

        }
    }
}
