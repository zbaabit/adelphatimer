﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Http;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using Adelphatimer1._0.Security;
using Adelphatimer1._0.Models;
using System.Web.Optimization;


namespace AdelphaTimer1._0
{
    public class MvcApplication : System.Web.HttpApplication
    {
        public MvcApplication()
        {
            this.AuthenticateRequest += Application_Authenticate;
        }


        protected void Application_Start()
        {
            //var keys = this.Application...AllKeys; //.Configuration.Formatters.JsonFormatter.S‌​erializerSettings.PreserveReferencesHandling = Newtonsoft.Json.PreserveReferencesHandling.None;
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        private void Application_Authenticate(object sender, EventArgs e)
        {
            // on recupere vle nom du cookie d'authentication forms
            string cookieName = FormsAuthentication.FormsCookieName;
            // on cherche ca presence dans la collection des cockie de la requete 
            HttpCookie authCookie = Context.Request.Cookies[cookieName];

            if (null == authCookie)
            {
                // indisponible
                return;
            }

            FormsAuthenticationTicket authTicket = null;
            try
            {
                // on essaye de decrypter le contenu cookie
                authTicket = FormsAuthentication.Decrypt(authCookie.Value);
            }
            catch (Exception ex)
            {
                // partie pour la journalisation
                return;
            }

            if (null == authTicket)
            {
                return;
            }
            string loginMail = authTicket.UserData.Split(';')[0]; // on recupre le nom d'utilisateur precedemment stocker comme userData lors du login
            string role = authTicket.UserData.Split(';')[1]; // et puis on recupere le role 

            FormsIdentity id = new FormsIdentity(authTicket); // on cree une identite forms a partir des infos qu'on a 

            // puis on cree un principal object qui va represente notre identite

            CustomPrincipal principal = new CustomPrincipal(id, role); // a jetter un oeuil sur cette classe 
            principal.User = new Resources() { LoginMail = loginMail };
            // associer la principal a l'utilisateur de context courant
            Context.User = principal;
        }
    }
}
