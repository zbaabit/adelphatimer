﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Adelphatimer1._0.Models;

namespace Adelphatimer1._0.Controllers
{
    public class SprintsController : Controller
    {
        private Entities db = new Entities();

        // GET: Sprints
        public ActionResult Index()
        {
            return View(db.Sprints.ToList());
        }

        // GET: Sprints/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Sprints sprints = db.Sprints.Find(id);
            if (sprints == null)
            {
                return HttpNotFound();
            }
            return View(sprints);
        }

        // GET: Sprints/Create
        public ActionResult Create(int id)
        {
            var sprint = new Sprints
            {
                CurrentDate = DateTime.Now,
            };
            ViewBag.StartDateProject = db.Projects.Where(p => p.Id == id).Select(p => p.PlannedStartDate);
            ViewBag.EndDateProject = db.Projects.Where(p => p.Id == id).Select(p => p.PlannedEndDate);
            return View(sprint);
        }


        // POST: Sprints/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,IdSprint,SprintName,StartDate,EndDate,EnabledRow,CurrentDate")] Sprints sprints)
        {
            if (ModelState.IsValid)
            {

                db.Sprints.Add(sprints);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(sprints);
        }

        // GET: Sprints/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Sprints sprints = db.Sprints.Find(id);
            if (sprints == null)
            {
                return HttpNotFound();
            }
            return View(sprints);
        }

        // POST: Sprints/Edit/5
        // Afin de déjouer les attaques par sur-validation, activez les propriétés spécifiques que vous voulez lier. Pour 
        // plus de détails, voir  http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,IdSprint,SprintName,StartDate,EndDate,EnabledRow,CurrentDate")] Sprints sprints)
        {
            if (ModelState.IsValid)
            {
                Sprints original = db.Sprints.AsNoTracking().FirstOrDefault(r => r.Id == sprints.Id);
                Sprints update = new Sprints();

                if (TryUpdateModel(update, "", new string[] { "IdSprint", "SprintName", "StartDate", "EndDate", "EnabledRow", "CurrentDate" }))
                {
                    original.EnabledRow = false;
                    update.SprintName = sprints.SprintName;
                    update.StartDate = sprints.StartDate;
                    update.EndDate = sprints.EndDate;
                    update.CurrentDate = DateTime.Now;

                    db.Entry(original).State = EntityState.Modified;
                    db.Sprints.Add(update);
                    db.SaveChanges();
                    return RedirectToAction("Index");

                }
            }
            return View(sprints);
        }


        // GET: Sprints/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Sprints sprints = db.Sprints.Find(id);
            if (sprints == null)
            {
                return HttpNotFound();
            }
            return View(sprints);
        }

        // POST: Sprints/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Sprints sprints = db.Sprints.Find(id);
            db.Sprints.Remove(sprints);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
