﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Adelphatimer1._0.Models;

namespace Adelphatimer1._0.Controllers
{
    public class StoriesController : Controller
    {
        private Entities db = new Entities();

        // GET: Stories
        public ActionResult Index(int? id)
        {
            if (id ==null)
            {
                var story = db.Stories.Include(s => s.Categories).Include(s => s.Projects).Include(s => s.Resources);
                 return View(story.ToList());

            }
            else
            {
                var stories = db.Stories.Include(s => s.Categories).Include(s => s.Projects).Include(s => s.Resources).Where(s => s.ProjectsId == id);
                return View(stories.ToList());
            }
          
        }

        // GET: Stories/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Stories stories = db.Stories.Find(id);
            if (stories == null)
            {
                return HttpNotFound();
            }
            return View(stories);
        }

        // GET: Stories/Create
        public ActionResult Create(int id)
        {

            ViewBag.CategoriesId = new SelectList(db.Categories, "Id", "CategoryName");
            ViewBag.ProjectsId = new SelectList(db.Projects, "Id", "ProjectName", selectedValue: id);
            ViewBag.ResourcesId = new SelectList(db.Resources, "Id", "LoginMail");
            ViewBag.RequestBy = new SelectList(db.Resources, "Id", "LoginMail");
            ViewBag.ValidateBy = new SelectList(db.Resources, "Id", "LoginMail");
            return View();
        }

        // POST: Stories/Create
        // Afin de déjouer les attaques par sur-validation, activez les propriétés spécifiques que vous voulez lier. Pour 
        // plus de détails, voir  http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,IdStory,StoryName,ResourcesId,Importance,InitialEstimate,Demonstrate,Notes,CategoriesId,RequestDate,RequestBy,ValidateDate,ValidateBy,EnabledRow,CurrentDate,ProjectsId")] Stories stories, int id)
        {
            if (ModelState.IsValid)
            {
                stories.ProjectsId = id;
                db.Stories.Add(stories);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.RequestBy = new SelectList(db.Resources, "Id", "LoginMail", stories.RequestBy);
            ViewBag.ValidateBy = new SelectList(db.Resources, "Id", "LoginMail", stories.ValidateBy);
            ViewBag.CategoriesId = new SelectList(db.Categories, "Id", "CategoryName", stories.CategoriesId);
            ViewBag.ProjectsId = new SelectList(db.Projects, "Id", "ProjectName", selectedValue: stories.ProjectsId);
            ViewBag.ResourcesId = new SelectList(db.Resources, "Id", "LoginMail", stories.ResourcesId);
            return View(stories);
        }

        // GET: Stories/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Stories stories = db.Stories.Find(id);
            if (stories == null)
            {
                return HttpNotFound();
            }
            ViewBag.RequestBy = new SelectList(db.Resources, "Id", "LoginMail", stories.RequestBy);
            ViewBag.ValidateBy = new SelectList(db.Resources, "Id", "LoginMail", stories.ValidateBy);
            ViewBag.CategoriesId = new SelectList(db.Categories, "Id", "CategoryName", stories.CategoriesId);
            ViewBag.ProjectsId = new SelectList(db.Projects, "Id", "ProjectName", stories.ProjectsId);
            ViewBag.ResourcesId = new SelectList(db.Resources, "Id", "LoginMail", stories.ResourcesId);
            return View(stories);
        }

        // POST: Stories/Edit/5
        // Afin de déjouer les attaques par sur-validation, activez les propriétés spécifiques que vous voulez lier. Pour 
        // plus de détails, voir  http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,IdStory,StoryName,ResourcesId,Importance,InitialEstimate,Demonstrate,Notes,CategoriesId,RequestDate,RequestBy,ValidateDate,ValidateBy,EnabledRow,CurrentDate,ProjectsId")] Stories stories)
        {
            if (ModelState.IsValid)
            {
                Stories original = db.Stories.AsNoTracking().FirstOrDefault(r => r.Id == stories.Id);
                Stories update = new Stories();
                if (TryUpdateModel(update, "", new string[] { "IdStory", "StoryName", "ResourcesId", "Importance", "InitialEstimate", "Demonstrate", "Notes", "CategoriesId", "RequestDate", "RequestBy", "ValidateDate", "ValidateBy", "EnabledRow", "CurrentDate", "ProjectsId" }))
                {
                    original.EnabledRow = false;

                    update.StoryName = stories.StoryName;
                    update.ResourcesId = stories.ResourcesId;
                    update.Importance = stories.Importance;
                    update.InitialEstimate = stories.InitialEstimate;
                    update.Demonstrate = stories.Demonstrate;
                    update.Notes = stories.Notes;
                    update.CategoriesId = stories.CategoriesId;
                    update.RequestDate = stories.RequestDate;
                    update.RequestBy = stories.RequestBy;
                    update.ValidateDate = stories.ValidateDate;
                    update.ValidateBy = stories.ValidateBy;
                    update.ProjectsId = stories.ProjectsId;
                    update.CurrentDate = DateTime.Now;

                    db.Entry(original).State = EntityState.Modified;
                    db.Stories.Add(update);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            ViewBag.RequestBy = new SelectList(db.Resources, "Id", "LoginMail", stories.RequestBy);
            ViewBag.ValidateBy = new SelectList(db.Resources, "Id", "LoginMail", stories.ValidateBy);
            ViewBag.CategoriesId = new SelectList(db.Categories, "Id", "CategoryName", stories.CategoriesId);
            ViewBag.ProjectsId = new SelectList(db.Projects, "Id", "ProjectName", stories.ProjectsId);
            ViewBag.ResourcesId = new SelectList(db.Resources, "Id", "LoginMail", stories.ResourcesId);
            return View(stories);
        }

        // GET: Stories/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Stories stories = db.Stories.Find(id);
            if (stories == null)
            {
                return HttpNotFound();
            }
            return View(stories);
        }

        // POST: Stories/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Stories stories = db.Stories.Find(id);
            db.Stories.Remove(stories);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
