﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Adelphatimer1._0.Models;

namespace Adelphatimer1._0.Controllers
{
    public class ResourcesController : Controller
    {
        private Entities db = new Entities();

        // GET: Resources


        public ActionResult Index(int? id)
        {
            if (id == null)
            {
                var resources = db.Resources.Include(r => r.RolesResources);
                return View(resources.ToList());

            }
            else
            {
                var resources = (from t in db.Teams
                                 where t.Id == id
                                 select t).FirstOrDefault().Resources.ToList();
                //db.Resources.Include(r => r.RolesResources).Include(t => t.Teams).Where(t => t.Teams.);
                return View(resources);
            }

        }

        // GET: Resources/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Resources resources = db.Resources.Find(id);
            if (resources == null)
            {
                return HttpNotFound();
            }
            return View(resources);
        }

        // GET: Resources/Create
        public ActionResult Create()
        {
            ViewBag.RolesResourcesId = new SelectList(db.RolesResources, "Id", "RoleResourceName");
            return View();
        }

        // POST: Resources/Create
        // Afin de déjouer les attaques par sur-validation, activez les propriétés spécifiques que vous voulez lier. Pour 
        // plus de détails, voir  http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,IdResource,FirstName,LastName,Initials,PhoneNumber,BirthDate,EnrollmentDate,LoginMail,Passwords,RolesResourcesId,EnabledRow,CurrentDate")] Resources resources)
        {
            if (ModelState.IsValid)
            {
                db.Resources.Add(resources);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.RolesResourcesId = new SelectList(db.RolesResources, "Id", "RoleResourceName", resources.RolesResourcesId);
            return View(resources);
        }

        // GET: Resources/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Resources resources = db.Resources.Find(id);
            if (resources == null)
            {
                return HttpNotFound();
            }
            ViewBag.RolesResourcesId = new SelectList(db.RolesResources, "Id", "RoleResourceName", resources.RolesResourcesId);
            return View(resources);
        }

        // POST: Resources/Edit/5
        // Afin de déjouer les attaques par sur-validation, activez les propriétés spécifiques que vous voulez lier. Pour 
        // plus de détails, voir  http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,IdResource,FirstName,LastName,PhoneNumber,BirthDate,EnrollmentDate,LoginMail,Passwords,RolesResourcesId,EnabledRow,CurrentDate")] Resources resources)
        {
            if (ModelState.IsValid)
            {
                Resources original = db.Resources.AsNoTracking().FirstOrDefault(r => r.Id == resources.Id);
                Resources update = new Resources();

                if (TryUpdateModel(update, "", new string[] { "IdResource", "FirstName", "LastName", "PhoneNumber", "BirthDate", "EnrollmentDate", "LoginMail", "Passwords", "IdRoleResource", "EnabledRow", "CurrentDate" }))
                {
                    original.EnabledRow = false;

                    update.FirstName = resources.FirstName;
                    update.LastName = resources.LastName;
                    update.PhoneNumber = resources.PhoneNumber;
                    update.BirthDate = resources.BirthDate;
                    update.EnrollmentDate = resources.EnrollmentDate;
                    update.LoginMail = resources.LoginMail;
                    update.Passwords = resources.Passwords;
                    update.RolesResourcesId = resources.RolesResourcesId;
                    update.CurrentDate = DateTime.Now;

                    db.Entry(original).State = EntityState.Modified;
                    db.Resources.Add(update);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            ViewBag.RolesResourcesId = new SelectList(db.RolesResources, "Id", "RoleResourceName", resources.RolesResourcesId);
            return View(resources);
        }

        // POST: Resources/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Resources resources = db.Resources.Find(id);
            db.Resources.Remove(resources);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
