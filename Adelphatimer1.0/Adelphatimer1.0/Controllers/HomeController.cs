﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Adelphatimer1._0.Models;
using System.Web.Security;

namespace Adelphatimer1._0.Controllers
{
    [Authorize(Roles = "user,admin")]
    public class HomeController : Controller
    {
        private Entities db = new Entities();
        // GET: Home
        public ActionResult Index()
        {
            var tasks = db.Tasks.Where(t => t.EstimateStartDate <= DateTime.Today && t.EstimateEndDate.Value >= DateTime.Today);
            return View(tasks.ToList());
        }
        public ActionResult LogOut()
        {
            FormsAuthentication.SignOut();

            return Redirect("/home/index");
        }
    }
}