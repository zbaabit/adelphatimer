﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Adelphatimer1._0.Models;
using AdelphaTimer1._0.ViewModels;

namespace Adelphatimer1._0.Controllers
{
    public class TeamsController : Controller
    {
        private Entities db = new Entities();

        // GET: Teams
        public ActionResult Index()
        {
            return View(db.Teams.ToList());
        }

        // GET: Teams/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Teams teams = db.Teams.Find(id);
            if (teams == null)
            {
                return HttpNotFound();
            }
            return View(teams);
        }

        // GET: Teams/Create
        public ActionResult Create()
        {
            var team = new Teams
            {
                CurrentDate = DateTime.Now,

            };
            return View(team);
        }

        // POST: Teams/Create
        // Afin de déjouer les attaques par sur-validation, activez les propriétés spécifiques que vous voulez lier. Pour 
        // plus de détails, voir  http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,IdTeam,TeamName,EnabledRow,CurrentDate")] Teams teams)
        {
            if (ModelState.IsValid)
            {
                db.Teams.Add(teams);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(teams);
        }



        public ActionResult AddResources(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var ressourcesteamsVM = new ResourcesTeamsViewModel
            {
                Team = db.Teams.Include(i => i.Resources).First(i => i.Id == id)
            };

            if (ressourcesteamsVM.Team == null)
            { return HttpNotFound(); }
            var allressourcesList = db.Resources.Where(rs => rs.EnabledRow == true).ToList();

            ressourcesteamsVM.AllResources = allressourcesList.Select(r => new SelectListItem
            {
                Text = r.LoginMail,
                Value = r.Id.ToString()
            });

            return View(ressourcesteamsVM);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddResources(ResourcesTeamsViewModel resourcesTeamsViewModel)
        {
            if (ModelState.IsValid)
            {
                if (resourcesTeamsViewModel == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                var TeamToUpdate = db.Teams.Include(i => i.Resources).First(i => i.Id == resourcesTeamsViewModel.Team.Id);

                if (TryUpdateModel(TeamToUpdate, "Team", new string[] { "LoginMail" }))
                {
                    var original = db.Teams.Include(i => i.Resources).First(i => i.Id == resourcesTeamsViewModel.Team.Id && i.EnabledRow == true);
                    var newRessource = db.Resources.Where(m => resourcesTeamsViewModel.SelectedResources.Contains(m.Id)).ToList();
                    var updatedRessources = new HashSet<int>(resourcesTeamsViewModel.SelectedResources);
                    foreach (Resources ressource in db.Resources)
                    {
                        if (!updatedRessources.Contains(ressource.Id))
                        {
                            original.EnabledRow = false;
                        }
                        else
                        {
                            TeamToUpdate.Resources.Add((ressource));
                        }
                    }

                    db.Entry(TeamToUpdate).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                }

                return RedirectToAction("Index");
            }
            return View(resourcesTeamsViewModel);
        }



        // GET: Teams/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var ressourcesteamsVM = new ResourcesTeamsViewModel
            {
                Team = db.Teams.Include(i => i.Resources).First(i => i.Id == id)
            };

            if (ressourcesteamsVM.Team == null)
                return HttpNotFound();
            var allressourcesList = db.Resources.Where(rs => rs.EnabledRow == true).ToList();

            ressourcesteamsVM.AllResources = allressourcesList.Select(r => new SelectListItem
            {
                Text = r.LoginMail,
                Value = r.Id.ToString()
            });

            return View(ressourcesteamsVM);
        }

        // POST: Teams/Edit/5
        // Afin de déjouer les attaques par sur-validation, activez les propriétés spécifiques que vous voulez lier. Pour 
        // plus de détails, voir  http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ResourcesTeamsViewModel teams)
        {
            if (teams == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            if (ModelState.IsValid)
            {
                var original = db.Teams.Include(i => i.Resources).First(i => i.Id == teams.Team.Id && i.EnabledRow == true);
                Teams TeamToUpdate = new Teams
                {
                    CurrentDate = DateTime.Now,
                    EnabledRow = true,
                    TeamName = teams.Team.TeamName
                };



                if (TryUpdateModel(TeamToUpdate, "Team", new string[] { "LoginMail" }))
                {
                    var newRessource = db.Resources.Where(m => teams.SelectedResources.Contains(m.Id)).ToList();
                    var updatedRessources = new HashSet<int>(teams.SelectedResources);
                    foreach (Resources ressource in db.Resources)
                    {
                        if (!updatedRessources.Contains(ressource.Id))
                        {
                            original.EnabledRow = false;
                        }
                        else
                        {
                            TeamToUpdate.CurrentDate = DateTime.Now;
                            TeamToUpdate.Resources.Add((ressource));
                        }
                    }

                    //db.Entry(original).State = System.Data.Entity.EntityState.Modified;
                    db.Teams.Add(TeamToUpdate);
                    db.SaveChanges();
                }

                return RedirectToAction("Index");
            }
            return View(teams);
        }

        // GET: Teams/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Teams teams = db.Teams.Find(id);
            if (teams == null)
            {
                return HttpNotFound();
            }
            return View(teams);
        }

        // POST: Teams/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Teams teams = db.Teams.Find(id);
            db.Teams.Remove(teams);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
