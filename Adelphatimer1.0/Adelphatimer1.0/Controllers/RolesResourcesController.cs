﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Adelphatimer1._0.Models;

namespace Adelphatimer1._0.Controllers
{
    public class RolesResourcesController : Controller
    {
        private Entities db = new Entities();

        // GET: RolesResources
        public ActionResult Index()
        {
            return View(db.RolesResources.ToList());
        }

        // GET: RolesResources/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RolesResources rolesResources = db.RolesResources.Find(id);
            if (rolesResources == null)
            {
                return HttpNotFound();
            }
            return View(rolesResources);
        }

        // GET: RolesResources/Create
        public ActionResult Create()
        {
            var roleResource = new RolesResources
            {
                CurrentDate = DateTime.Now,
            };
            return View(roleResource);
        }

        // POST: RolesResources/Create
        // Afin de déjouer les attaques par sur-validation, activez les propriétés spécifiques que vous voulez lier. Pour 
        // plus de détails, voir  http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,IdRoleResource,RoleResourceName,EnabledRow,CurrentDate")] RolesResources rolesResources)
        {
            if (ModelState.IsValid)
            {
                /*if (db.RolesResources.Any(i => i.IdRoleResource == rolesResources.IdRoleResource))
                    {
                    return Json(new { status = "error", message = "Id already exists" });
                }*/
                db.RolesResources.Add(rolesResources);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(rolesResources);
        }

        // GET: RolesResources/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RolesResources rolesResources = db.RolesResources.Find(id);
            if (rolesResources == null)
            {
                return HttpNotFound();
            }
            return View(rolesResources);
        }


        // POST: RolesResources/Edit/5
        // Afin de déjouer les attaques par sur-validation, activez les propriétés spécifiques que vous voulez lier. Pour 
        // plus de détails, voir  http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,IdRoleResource,RoleResourceName,EnabledRow,CurrentDate")] RolesResources rolesResource)
        {
            if (ModelState.IsValid)
            {

                RolesResources original = db.RolesResources.AsNoTracking().FirstOrDefault(r => r.Id == rolesResource.Id);
                RolesResources update = new RolesResources();

                if (TryUpdateModel(update, "", new string[] { "RoleResourceName", "EnabledRow", "CurrentDate" }))
                {
                    original.EnabledRow = false;

                    update.RoleResourceName = rolesResource.RoleResourceName;
                    update.CurrentDate = DateTime.Now;

                    db.Entry(original).State = EntityState.Modified;
                    db.RolesResources.Add(update);
                    db.SaveChanges();
                    return RedirectToAction("Index");

                }
            }
            return View(rolesResource);
        }


        // GET: RolesResources/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RolesResources rolesResources = db.RolesResources.Find(id);
            if (rolesResources == null)
            {
                return HttpNotFound();
            }
            return View(rolesResources);
        }

        // POST: RolesResources/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            RolesResources rolesResources = db.RolesResources.Find(id);
            db.RolesResources.Remove(rolesResources);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
