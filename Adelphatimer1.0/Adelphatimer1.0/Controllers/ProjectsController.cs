﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Adelphatimer1._0.Models;
using AdelphaTimer1._0.ViewModels;
using Adelphatimer1._0.Helpers;


namespace Adelphatimer1._0.Controllers
{
    public class ProjectsController : Controller
    {
        private Entities db = new Entities();
        private double GetAdvancement(double val1, double val2)
        {
            double result = ((val1 - val2) / val1) * 100;

            return Math.Round(result, 2);
        }
        private double GetCharge(double val1, double val2, double val3)
        {
            double result = ((val1 + val2) - val3) / val3;
            return Math.Round(result, 2); ;
        }
        // GET: Projects
        public ActionResult Index()
        {
            var projects = db.Projects.Include(p => p.Sponsors).Include(nameof(Resources)).Where(p => p.EnabledRow == true);

            return View(projects.ToList());
        }

        // GET: Projects/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Projects projects = db.Projects.Find(id);
            if (projects == null)
            {
                return HttpNotFound();
            }
            return View(projects);
        }

        // GET: Projects/Create
        public ActionResult Create()
        {
            var project = new Projects
            {
                CurrentDate = DateTime.Now,
                Advancement = 0,
                EcartCharge = 0,
                EcartDuree = 0,
                Statute = "Initiation"
            };

            ViewBag.SponsorsId = new SelectList(db.Sponsors, "Id", "SponsorName");
            return View(project);
        }

        // POST: Projects/Create
        // Afin de déjouer les attaques par sur-validation, activez les propriétés spécifiques que vous voulez lier. Pour 
        // plus de détails, voir  http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,IdProject,ProjectName,ProjectDescription,Manager,PlannedStartDate,PlannedEndDate,PlannedWork,UpdateDate,ActualWork,ToDo,FTE,UpdateEndDate,Advancement,EcartCharge,EcartDuree,Statute,Drive,EnabledRow,CurrentDate,SponsorsId")] Projects projects)
        {
            if (ModelState.IsValid)
            {

                projects.Advancement = 0;
                projects.EcartCharge = 0;
                projects.EcartDuree = 0;
                //projects.EcartDuree = projects.UpdateDate.Value.BusinessDaysUntil(projects.PlannedEndDate.Value).ToString();
                projects.Statute = "Initiation";
                db.Projects.Add(projects);
                db.SaveChanges();
                return RedirectToAction("Index");
            }


            ViewBag.SponsorsId = new SelectList(db.Sponsors, "Id", "SponsorName", projects.SponsorsId);
            return View(projects);
        }

        // GET: Projects/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var projectresourcesVM = new ProjectsResourcesViewModel
            {
                Project = db.Projects.Include(i => i.Resources).First(i => i.Id == id)
            };
            if (projectresourcesVM.Project == null)
            {
                return HttpNotFound();
            }
            var allressourcesList = db.Resources.Where(rs => rs.EnabledRow == true).ToList();

            projectresourcesVM.AllResources = allressourcesList.Select(r => new SelectListItem
            {
                Text = r.LoginMail,
                Value = r.Id.ToString()
            });


            ViewBag.Manager = new SelectList(db.Resources, "Id", "LoginMail", projectresourcesVM.Project.Manager);
            ViewBag.SponsorsId = new SelectList(db.Sponsors, "Id", "SponsorName", projectresourcesVM.Project.SponsorsId);
            return View(projectresourcesVM);
        }

        // POST: Projects/Edit/5
        // Afin de déjouer les attaques par sur-validation, activez les propriétés spécifiques que vous voulez lier. Pour 
        // plus de détails, voir  http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ProjectsResourcesViewModel projectsVM)
        {
            if (projectsVM == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            if (ModelState.IsValid)
            {
                //var original = db.Projects.Include(i => i.Resources).FirstOrDefault(i => i.Id == projects.Project.Id && i.EnabledRow == true);
                var origine = db.Projects.Where(y => y.Id.Equals(projectsVM.Project.Id)).FirstOrDefault();
                var ProjectToUpdate = db.Projects.Include(i => i.Resources).First(i => i.Id == projectsVM.Project.Id);

                origine.EnabledRow = false;
                if (TryUpdateModel(ProjectToUpdate, "Project", new string[] { "LoginMail" }))
                {

                    var newRessource = db.Resources.Where(m => projectsVM.SelectedResources.Contains(m.Id)).ToList();
                    var updatedRessources = new HashSet<int>(projectsVM.SelectedResources);
                    foreach (Resources ressource in db.Resources)
                    {
                        if (!updatedRessources.Contains(ressource.Id))
                        {
                            origine.EnabledRow = false;
                        }
                        else
                        {
                            origine.EnabledRow = false;
                            ProjectToUpdate.ProjectNumber = origine.ProjectNumber;
                            ProjectToUpdate.SponsorsId = origine.SponsorsId;
                            ProjectToUpdate.EnabledRow = true;
                            ProjectToUpdate.ProjectName = projectsVM.Project.ProjectName;
                            ProjectToUpdate.ProjectDescription = projectsVM.Project.ProjectDescription;
                            ProjectToUpdate.Manager = projectsVM.Manager;
                            ProjectToUpdate.PlannedStartDate = projectsVM.Project.PlannedStartDate;
                            ProjectToUpdate.PlannedWork = projectsVM.Project.PlannedWork;
                            ProjectToUpdate.PlannedEndDate = projectsVM.Project.PlannedEndDate;
                            ProjectToUpdate.UpdateDate = projectsVM.Project.UpdateDate;
                            ProjectToUpdate.ActualWork = projectsVM.Project.ActualWork;
                            ProjectToUpdate.ToDo = projectsVM.Project.ToDo;
                            ProjectToUpdate.FTE = projectsVM.Project.FTE;
                            ProjectToUpdate.UpdateEndDate = projectsVM.Project.UpdateEndDate;
                            ProjectToUpdate.Advancement = GetAdvancement(projectsVM.Project.PlannedWork, projectsVM.Project.ToDo);
                            ProjectToUpdate.EcartCharge = GetCharge(projectsVM.Project.ActualWork, projectsVM.Project.ToDo, projectsVM.Project.PlannedWork);
                            ProjectToUpdate.EcartDuree = projectsVM.Project.UpdateEndDate.Value.GetWorkingDays(projectsVM.Project.PlannedEndDate.Value);
                            ProjectToUpdate.Statute = projectsVM.SelectedStatute;
                            ProjectToUpdate.Drive = projectsVM.Project.Drive;
                            ProjectToUpdate.CurrentDate = DateTime.Now;
                            ProjectToUpdate.Resources.Add((ressource));
                        }
                    }
                    db.Projects.Add(ProjectToUpdate);
                    db.SaveChanges();
                }
                return RedirectToAction("Index");
            }

            ViewBag.Manager = new SelectList(db.Resources, "LoginMail", "LoginMail", projectsVM.Manager);
            ViewBag.SponsorsId = new SelectList(db.Sponsors, "Id", "SponsorName", projectsVM.Project.SponsorsId);
            return View(projectsVM);
        }


        // GET: Projects/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Projects projects = db.Projects.Find(id);
            if (projects == null)
            {
                return HttpNotFound();
            }
            return View(projects);
        }

        // POST: Projects/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Projects projects = db.Projects.Find(id);
            db.Projects.Remove(projects);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
