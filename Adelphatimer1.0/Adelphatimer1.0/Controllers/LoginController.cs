﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Adelphatimer1._0.Models;
using AdelphaTimer1._0.ViewModels;

namespace AdelphaTimer0._5.Controllers
{
    public class LoginController : Controller
    {
        Entities _context;

        public LoginController()
        {
            _context = new Entities();
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(ResourcesLoginViewModel model)
        {
            if (!ModelState.IsValid) return View(model);

            // on recupere depuis le DbContext l'utilisateur ayant le username et password qui coincide a ceux envoyer 
            var user = _context.Resources.Include("RolesResources").FirstOrDefault(u => u.LoginMail == model.LoginMail && u.Passwords == model.Password);
            // si on a un resultat 
            if (user != null)
            {
                // on cree un ticket d'authentification Forms 
                // Note: sur le fichier web.config sur la partie <authentication mode="Forms"/>
                // sert a specifier le type d'authentification a utiliser
                FormsAuthenticationTicket ticket =
                new FormsAuthenticationTicket(version: 1, name: model.LoginMail,
                                        issueDate: DateTime.Now, expiration: DateTime.Now.AddMinutes(20),
                                        isPersistent: false,
                                        userData: String.Format("{0};{1}", user.LoginMail, user.RolesResources.RoleResourceName));
                // on crypte le ticket 
                string encryptedTicket = FormsAuthentication.Encrypt(ticket);
                // et on en cree un cookie
                HttpCookie authCookie =
                             new HttpCookie(FormsAuthentication.FormsCookieName,
                                            encryptedTicket);
                // puis on l'ajoute a la liste de cookie 
                Response.Cookies.Add(authCookie);
                // le reste de la logique se deroule sur le fichier Global.asax.cs
                //Response.Redirect();
              
                var returnUrl = FormsAuthentication.GetRedirectUrl(user.LoginMail, false);
                // return Redirect(returnUrl);
                if (returnUrl.ToString() == "/")
                {
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    var controller = returnUrl.Split('/')[1];
                    var action = returnUrl.Split('/')[2];
                    return RedirectToAction(action, controller);
                }

            }
            ModelState.AddModelError("Username", "Authentication Failed");
            return View(model);
        }
        

    }
}