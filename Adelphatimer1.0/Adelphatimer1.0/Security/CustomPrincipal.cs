﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Security.Principal;
using Adelphatimer1._0.Models;

namespace  Adelphatimer1._0.Security
{
    public class CustomPrincipal: IPrincipal
    {
        private IIdentity _identity;
        private Entities _context;
        private string _role;

        public CustomPrincipal(IIdentity identity)
        {
            _identity = identity;
            _context = new Entities();
        }

        public CustomPrincipal(IIdentity identity, string role) : this(identity)
        {
            this._role = role;
        }

        public Resources User { get; set; }

        public IIdentity Identity => _identity;
        public bool IsInRole(string role)
        {
            return !String.IsNullOrEmpty(_role)
                ? _role == role
                : _context.Resources.Include(nameof(RolesResources)).FirstOrDefault(u => u.LoginMail == User.LoginMail)?.RolesResources.RoleResourceName == role;
        }
    }
}