﻿using Adelphatimer1._0.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AdelphaTimer1._0.ViewModels
{
    public class ProjectsResourcesViewModel
    {
        public Projects Project { get; set; }

       public string Manager { get; set; }

        public IEnumerable<SelectListItem> AllResources { get; set; }

        private List<int> _selectedResources;
        public List<int> SelectedResources
        {
            get
            {
                if (_selectedResources == null)
                {
                    _selectedResources = Project.Resources?.Select(m => m.Id).ToList();
                }
                return _selectedResources;
            }
            set { _selectedResources = value; }
        }

        public string SelectedStatute { get; set; }
        public IEnumerable<SelectListItem> Statute
        {
            get
            {
                return new[]
                {
                new SelectListItem { Value = "Progress", Text = "In-Progress" },
                new SelectListItem { Value = "Fenced", Text = "Fenced" },
                new SelectListItem { Value = "Blocked", Text = "Blocked" },
            };
            }
        }
    }
}