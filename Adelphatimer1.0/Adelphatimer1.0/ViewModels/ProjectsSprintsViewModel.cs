﻿using Adelphatimer1._0.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdelphaTimer1._0.ViewModels
{
    public class ProjectsSprintsViewModel
    {
        public Sprints Sprints { get; set; }
        public Projects Projects { get; set; }
        public Resources Resources { get; set; }


       
        public List<Resources> AllResources { get; set; }
        public List<Sprints> AllSprints { get; set; }
        public List<Projects> AllProjects { get; set; }
    }
}