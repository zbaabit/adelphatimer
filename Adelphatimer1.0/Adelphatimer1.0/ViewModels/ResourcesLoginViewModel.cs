﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdelphaTimer1._0.ViewModels
{
    public class ResourcesLoginViewModel
    {
        public string LoginMail { get; set; }
        public string Password { get; set; }
    }
}