﻿using Adelphatimer1._0.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AdelphaTimer1._0.ViewModels
{
    public class ResourcesTeamsViewModel
    {
        public Teams Team { get; set; }
        public IEnumerable<SelectListItem> AllTeams { get; set; }
        public IEnumerable<SelectListItem> AllResources { get; set; }

        private List<int> _selectedResources;
        public List<int> SelectedResources
        {
            get
            {
                if (_selectedResources == null)
                {
                    _selectedResources = Team.Resources.Select(m => m.Id).ToList();
                }
                return _selectedResources;
            }
            set { _selectedResources = value; }
        }
    }
}